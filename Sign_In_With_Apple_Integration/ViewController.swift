//
//  ViewController.swift
//  Sign_In_With_Apple_Integration
//
//  Created by Amol Tamboli on 25/06/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit
import AuthenticationServices

class ViewController: UIViewController  {

    @IBOutlet weak var lblAppleId: UILabel!
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpAppleSignInbtn()
    }
    
    func setUpAppleSignInbtn(){
        let signInbtn = ASAuthorizationAppleIDButton()
        signInbtn.frame = CGRect(x: 20, y: (UIScreen.main.bounds.size.height - 100), width: (UIScreen.main.bounds.size.width - 40), height: 50)
        
        //MARK:- Button action
        signInbtn.addTarget(self, action: #selector(signInActionBtn), for: .touchUpInside)
        
        self.view.addSubview(signInbtn)
    }

    @objc func signInActionBtn(){
        print("Sign In with apple")
        let appleIdProvider = ASAuthorizationAppleIDProvider()
        let request = appleIdProvider.createRequest()
        request.requestedScopes = [.email,.fullName]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
}

extension ViewController : ASAuthorizationControllerDelegate{
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        let alert = UIAlertController(title: "Error", message: "\(error.localizedDescription)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let credentials as ASAuthorizationAppleIDCredential:
            print(credentials.user)
          //  print(credentials.fullName?.givenName)
          //  print(credentials.fullName?.familyName)
            print(credentials.fullName!)
            print(credentials.email!)

            
            //MARK:- Populate data on screen
            self.lblAppleId.text = credentials.user
            self.lblFirstName.text = credentials.fullName?.givenName
            self.lblLastName.text = credentials.fullName?.familyName
            self.lblEmail.text = credentials.email

        case let credentials as ASPasswordCredential:
            print(credentials.password)
        default:
                    let alert = UIAlertController(title: "Apple SignIn", message: "Something went wrong with your Apple SignIN", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok", style: .cancel, handler: nil))
        }
    }
}

extension ViewController : ASAuthorizationControllerPresentationContextProviding{
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor{
        return view.window!
    }

}
